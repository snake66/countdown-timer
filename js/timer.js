var count = 12 * 60;
var counter;
var counting = false;

var startbtn = document.getElementById("start");
startbtn.onclick = startTimer;
startbtn.removeAttribute("disabled");

var stopbtn = document.getElementById("stop");
stopbtn.onclick = stopTimer;

var adjustdlg = document.getElementById("adjust");
adjustdlg.style.visibility = 'hidden';

var setbtn = document.getElementById("set");
setbtn.onclick = toggleAdjustDialog;

document.getElementById("min-down").onclick = decrementMin;
document.getElementById("min-up").onclick = incrementMin;
document.getElementById("sec-down").onclick = decrementSec;
document.getElementById("sec-up").onclick = incrementSec;

updateTime();

function startTimer()
{
  if (!counting)
  {
    counting = true;
    startbtn.setAttribute("disabled", true);
    setbtn.setAttribute("disabled", true);
    adjustdlg.style.visibility = 'hidden';
    counter = setInterval(timer, 1000); //1000 will  run it every 1 second
  }
}

function stopTimer()
{
  if (!counting)
  {
    count = 12 * 60;
    updateTime();
  }
  else
  {
    clearInterval(counter);
    counting = false;
  }
  startbtn.removeAttribute("disabled");
  setbtn.removeAttribute("disabled");
}

function tostr(num)
{
  var str = "";
  if (num < 10)
    str += "0";
  str += num;

  return str;
}

function updateTime()
{
  var d = new Date(count * 1000);
  var t = document.getElementById("timer");

  t.innerHTML = tostr(d.getMinutes()) + ":" + tostr(d.getSeconds());
}

function timer()
{
  count=count-1;
  if (count <= 0)
  {
    stopTimer();
    return;
  }

  updateTime();
}

function toggleAdjustDialog()
{
  if (adjustdlg.style.visibility == 'hidden')
    adjustdlg.style.visibility = "visible";
  else
    adjustdlg.style.visibility = 'hidden';
}

function incrementMin()
{
  count += 60;
  updateTime();
}

function decrementMin()
{
  count -= 60;
  updateTime();
}

function incrementSec()
{
  count += 1;
  updateTime();
}

function decrementSec()
{
  count -= 1;
  updateTime();
}
