# Javascript countdown timer

Just an ultrasimple timer counting down from a given number of minutes and seconds. I originally needed this to keep track of remaining time for a short talk I did. Feel free to fork, adapt and make it useful to you.

## License

Copyright (C) 2015  Harald Eilertsen <haraldei@anduin.net>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>
